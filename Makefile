all: container

# 0.0 shouldn't clobber any release builds
TAG = 0.2
PREFIX = remotejob/nginx-ingress

controller: controller.go
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-w' -o controller ./controller.go

container: controller
	docker build -t $(PREFIX):$(TAG) .

push: 
	docker push $(PREFIX):$(TAG)

clean:
	rm -f controller
